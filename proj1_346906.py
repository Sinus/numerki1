import collections
import numpy as np
import scipy as sp

author = "Marcin Dominiak"
python_version = "3.4.3"
numpy_version = "1.8.2"
opis_struktur = \
"Macierz permutacji P jest trzymana jako słownik int -> int.\n\
P[i] = j znaczy, że zamieniono miejscami wiersze i-ty z j-tym.\n\
Macierze L i U są równierz słownikami int -> (mapa int -> numpy.float_).\n\
L[i] jest mapą reprezentującą i-tą kolumnę macierzy U, natomiast\n\
L[i][j] jest wartością w i-tej kolumnie w j-tym wierszu.\n\
Macierz U jest reprezentowana tak samo jak L.\n\
W obu macierzach dla oszczędności pamięci pamiętane są tylko elementy\n\
niezerowe, a w macierzy L nie są pamiętane również jedynki z diagonali.\n\
Dzięki temu złożoność pamięciowa tego programu to O(n) gdzie n jest liczbą\n\
wierszy w macierzy A.\n"

class _Matrix(dict):
    def __missing__(self, key):
        return 0

def _prepare(a, a_u, a_l):
    '''
    Zwraca macierz trójpasową w postacji kolumnowej
    tzn u[i] to i-ta kolumna macierzy a, u[i][x] = wartość w i-tej kolumnie
    w x-tym wierszu
    '''
    l = _Matrix()
    u = _Matrix()
    p = _Matrix()

    for i in range(0, len(a)):
        l[i] = _Matrix()
        u[i] = _Matrix()
        p[i] = i

    for i in range(0, len(a)):
        u[i][i] = np.float_(a[i])
        if i > 0:
            u[i - 1][i] = np.float_(a_l[i - 1])
        if i < len(a) - 1:
            u[i + 1][i] = np.float_(a_u[i])

    return l, u, p, len(a)

def _zamien_wiersz(A, x, y):
    '''
    Zamienia wiersz x-ty z y-tym w macierzy A
    '''
    kolumny = A.keys()
    for i in kolumny:
        A[i][x], A[i][y] = A[i][y], A[i][x]
        if np.isclose(A[i][x], 0):
            del A[i][x]
        if np.isclose(A[i][y], 0):
            del A[i][y]

def _wyborElGlownego(u, i, p, l, n):
    '''
    permutuje U wybierając w kolumnie i element główny
    '''
    print("wybor el głównego w kolumnie ", i)

    if i == n:# or i == n - 1:
        return

    kol = u[i]
    if kol == 0:
        return

    wiersze = kol.keys()

    maxWartosc = 0
    maxWiersz = -1

    for wiersz in wiersze:
        #O(1) bo wierszy jest nie więcej niż kilka
        if wiersz >= i and abs(u[i][wiersz]) > abs(maxWartosc):
            maxWartosc = u[i][wiersz]
            maxWiersz = wiersz

    if maxWiersz != i:
        #permutujemy P
        p[i], p[maxWiersz] = p[maxWiersz], p[i]

        #permutujemy U oraz L
        _zamien_wiersz(u, i, maxWiersz)
        _zamien_wiersz(l, i, maxWiersz)

def _redukuj(u, l, i, n):
    # pod rogiem jest maksymalnie 1 element
    # i-ta linia ma maksymalnie 3 elementy

    if i == n or i == n - 1:
        return

    if u[i] == 0:
        return

    wspolczynnik = u[i][i + 1] / u[i][i]

    if u[i][i + 1] != 0:
        del u[i][i + 1]

    u[i + 1][i + 1] -= wspolczynnik * u[i + 1][i]
    if i != n - 2:
        u[i + 2][i + 1] -= wspolczynnik * u[i + 2][i]

    if np.isclose(u[i + 1][i + 1], 0):
        del u[i + 1][i + 1]

    if i != n - 2:
        if np.isclose(u[i + 2][i + 1], 0):
            del u[i + 2][i + 1]

    l[i][i + 1] = wspolczynnik

def tridiag_lu(l, u, p):
    l, u, p, n = _prepare(l, u, p)

    for x in range(0, n):
        _wyborElGlownego(u, x, p, l, n)
        _redukuj(u, l, x, n)

    return p, l, u

def decode_tridiag_lu(p, l, u):
    Pout = np.zeros(shape=(len(p.keys()), len(p.keys())))
    Lout = np.zeros(shape=(len(p.keys()), len(p.keys())))
    Uout = np.zeros(shape=(len(p.keys()), len(p.keys())))
    for x in p.keys():
        Pout[x][p[x]] = 1

    for kol in u.keys():
        for wiersz in u[kol].keys():
            Uout[wiersz][kol] = u[kol][wiersz]

    for kol in l.keys():
        Lout[kol][kol] = 1
        for wiersz in l[kol].keys():
            Lout[wiersz][kol] = l[kol][wiersz]

    return Pout, Lout, Uout

def _permutuj_wektor(wektor, p):
    out = np.empty_like(wektor)
    out[:] = wektor
    #wektor == out
    #szybsze niż copyto

    for x in range(0, len(p)):
        out[x] = wektor[p[x]]
    return out

def _rozwiaz_tri_low(l, b):
    y = np.empty_like(b)

    #y[m] = b[m] - sum_{i = 0}^{m - 1}(l[i][m] * y[i])

    wspolczynniki = collections.defaultdict(dict)
    for kol in l.keys():
        for wier in l[kol].keys():
            wspolczynniki[wier][kol] = l[kol][wier]

    for i in range(0, len(b)):
        y[i] = b[i]
        for kolumna in wspolczynniki[i].keys():
            y[i] -= wspolczynniki[i][kolumna] * y[kolumna]

    return y

def _rozwiaz_tri_up(u, y):
    x = np.empty_like(y)

    wspolczynniki = collections.defaultdict(dict)
    for kol in u.keys():
        for wier in u[kol].keys():
            wspolczynniki[wier][kol] = u[kol][wier]

    i = len(y) - 1
    while i >= 0:
        x[i] = y[i]
        for j in wspolczynniki[i].keys():
            if j > i:
                x[i] -= wspolczynniki[i][j] * x[j]
        x[i] /= u[i][i]
        i -= 1

    return x


def tridiag_solve(p, l, u, b):
    '''
    Mając dany rozkład PA = LU rozwiązuje układ równan Ax = b
    Używa metody Ly = Pb and Ux = y
    '''
    pb = _permutuj_wektor(b, p)

    y = _rozwiaz_tri_low(l, pb)

    x = _rozwiaz_tri_up(u, y)

    return x

if __name__ == '__main__':
    print(opis_struktur)
